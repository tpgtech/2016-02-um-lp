# README #

1) Install Node
  https://nodejs.org/en/

2) Install LiveReload browser extension
  http://livereload.com/
  https://chrome.google.com/webstore/detail/livereload/jnihajbhpnppcggbcgedagnkighmdlei?hl=en

3) Run setup.sh in an empty directory (script downloads repository files, installs Gulp dependencies, initializes Gulp, starts Gulp watch task).

4) Browse to HTML file in browser, enable LiveReload extension (click LiveRelod icon in extensions menu).

5) Do developer things. Gulp watches for changes in images, JavaScript, HTML, CSS, and SASS files and reloads the browser.
  'debug' in config.json pauses uglify task.

6) Profit!

Stop gulp task by entering CTRL+C in console.