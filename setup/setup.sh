#!/bin/bash
rm -rf * .*
echo "retreiving files"
git init
git remote add origin https://jonaddams@bitbucket.org/jonaddams/lp-build.git
git fetch origin
git checkout -b master --track origin/master 
echo "npm init..."
npm init
echo "installing gulp modules..."
npm install gulp fs gulp-autoprefixer gulp-babel gulp-beautify gulp-changed gulp-concat del gulp-html-replace gulp-if gulp-image-optimization gulp-inline-css gulp-livereload gulp-minify-css gulp-minify-html gulp-rename gulp-sass gulp-uglify request
echo "starting gulp"
gulp
gulp watch