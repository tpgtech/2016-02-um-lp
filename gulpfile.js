/*global
gulp
*/

/*jslint
es6,
*/
var getToken,
    logError,
    update,
    updateAsset,
    updateContent,
    gulp = require('gulp'),
    autoprefixer = require('gulp-autoprefixer'),
    babel = require('gulp-babel'),
    beautify = require('gulp-beautify'),
    concat = require('gulp-concat'),
    config = require('./config.json'),
    del = require('del'),
    fs = require('fs'),
    gulpif = require('gulp-if'),
    htmlreplace = require('gulp-html-replace'),
    imageOptimizer = require('gulp-image-optimization'),
    inlineCSS = require('gulp-inline-css'),
    livereload = require('gulp-livereload'),
    minifyCSS = require('gulp-minify-css'),
    minifyHTML = require('gulp-minify-html'),
    rename = require("gulp-rename"),
    request = require('request'),
    sass = require('gulp-sass'),
    uglify = require('gulp-uglify');

logError = function _logError(error, response, body, msg) {
    'use strict';
    console.log('Response code: ' + response.statusCode);
    console.log(msg);
    console.log(error);
    console.log(body);
};

getToken = function _getToken(callback) {
    'use strict';

    var url;

    url = config.marketo.marketoURL + '/identity/oauth/token?grant_type=client_credentials&client_id=' + config.marketo.clientId + '&client_secret=' + config.marketo.clientSecret;

    console.log('Requesting Marketo token...');
    request(url, function (error, response, body) {
        var jsonObj;

        if (response.statusCode === 200) {
            jsonObj = JSON.parse(body);
            console.log('Received Marketo token: ' + jsonObj.access_token);
            callback(jsonObj.access_token);
        } else {
            logError(error, response, body, 'Error retreiving token.');
        }
    });
};

update = function _update(type) {
    'use strict';

    if (config.marketo[type].update === true) {
        var updateAssetCallback = function _updateAssetCallback(token) {
            config.marketo[type].files.forEach(function (element, index, array) {
                updateAsset(token, element);
            });
        };

        getToken(updateAssetCallback);
    }
};

updateAsset = function _updateAsset(token, obj) {
    'use strict';

    var formData,
        url;

    url = config.marketo.marketoURL + '/rest/asset/v1/file/' + obj.id + '/content.json?access_token=' + token;

    formData = {
        file: fs.createReadStream(__dirname + obj.filename)
    };

    request.post({url: url, formData: formData}, function (error, response, body) {
        var jsonBody = JSON.parse(body);

        console.log('Updating file ' + obj.filename + ' ...');
        if (response.statusCode === 200 && jsonBody.success === true) {
            console.log('Successfully updated ' + obj.filename);
        } else {
            logError(error, response, body, 'Update failed.');
        }
    });
};

updateContent = function _updateContent(token, type, obj) {
    'use strict';

    var formData,
        url;

    url = config.marketo.marketoURL + '/rest/asset/v1/' + type + '/' + obj.id + '/content.json?access_token=' + token;

    formData = {
        content: fs.createReadStream(__dirname + obj.filename)
    };

    request.post({url: url, formData: formData}, function (error, response, body) {
        var jsonBody = JSON.parse(body);

        if (response.statusCode === 200 && jsonBody.success === true) {
            url = config.marketo.marketoURL + '/rest/asset/v1/' + type + '/' + obj.id + '/approveDraft.json?access_token=' + token;

            console.log('Approving template ' + obj.filename + ' ...');
            request.post(url, function (error, response, body) {
                if (response.statusCode === 200 && jsonBody.success === true) {
                    console.log('Successfully appoved ' + obj.filename);
                } else {
                    logError(error, response, body, 'Approval failed.');
                }
            });
        } else {
            logError(error, response, body, 'Update failed.');
        }
    });
};

gulp.task('cleanImages', function () {
    'use strict';
    return del(config.dest + '/img/*');
});

gulp.task('cleanHtml', function () {
    'use strict';
    return del(config.dest + '/*.html');
});

gulp.task('cleanScripts', function () {
    'use strict';
    return del(config.dest + '/js/*');
});

gulp.task('cleanStyles', function () {
    'use strict';
    return del(config.dest + '/styles/*');
});

// minify new images
gulp.task('images', ['cleanImages'], function () {
    'use strict';

    gulp.src([config.src + '/img/**/*.png', config.src + '/img/**/*.jpg', config.src + '/img/**/*.gif', config.src + '/img/**/*.jpeg', config.src + '/img/**/*.svg']).pipe(imageOptimizer({
        optimizationLevel: 5,
        progressive: true,
        interlaced: true
    })).pipe(gulp.dest(config.dest + '/img'));
});

// minify HTML
gulp.task('html', ['cleanHtml'], function () {
    'use strict';

    gulp.src(config.src + '/*.html')
        .pipe(htmlreplace({
            css: 'styles/styles.css',
            js: 'js/combined.js'
        }))
        // .pipe(minifyHTML())
        .pipe(gulp.dest(config.dest))
        .pipe(livereload());
});

// inline CSS (email templates)
gulp.task('inline', function () {
    'use strict';

    gulp.src(config.src + '/*.html')
        .pipe(inlineCSS({
            applyStyleTags: true, // Inline styles in <style></style>.
            removeStyleTags: false, // Remove the original <style></style> tags after (possibly) inlining the css from them.
            applyLinkTags: true, // Resolve <link rel="stylesheet"> tags and inline the resulting styles.
            removeLinkTags: true, // Remove the original <link rel="stylesheet"> tags after (possibly) inlining the css from them.
            preserveMediaQueries: true, // Preserves all media queries (and contained styles) within <style></style> tags as a refinement when removeStyleTags is true. Other styles are removed.
            applyWidthAttributes: true // Use any CSS pixel widths to create width attributes on elements.
        }))
        .pipe(rename({
            suffix: '-inline'
        }))
        .pipe(gulp.dest(config.src));
});

// compile sass
gulp.task('sass', function () {
    'use strict';

    gulp.src(config.src + '/styles/sass/*.scss')
        .pipe(sass())
        .pipe(gulp.dest(config.src + '/styles/css'));
});

// concat css, auto-prefix, and minify
gulp.task('styles', ['cleanStyles'], function () {
    'use strict';

    gulp.src([config.src + '/styles/css/*.css'])
        .pipe(concat('styles.css'))
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(minifyCSS())
        .pipe(gulp.dest(config.dest + '/styles/'))
        .pipe(livereload());
});

// babel es6/7 transpiler
gulp.task('babel', function () {
    'use strict';

    gulp.src([config.src + '/js/babel/*.js'])
        .pipe(babel())
        .pipe(rename({
            suffix: '-transpiled'
        }))
        .pipe(gulp.dest(config.src + '/js'));
});

// concat and minify JavaScript
gulp.task('scripts', ['cleanScripts'], function () {
    'use strict';

    var files = config.scripts.map(function (e) {
        return config.src + e;
    });

    console.log(files[0]);
    console.log(files[1]);

    gulp.src(files)
        .pipe(concat('combined.js'))
        // .pipe(gulpif(config.debug, beautify(), uglify()))
        .pipe(gulp.dest(config.dest + '/js/'))
        .pipe(livereload());
});

// replace js and css includes with one include statement
gulp.task('replace', function () {
    'use strict';

    gulp.src(config.src + '/index.html')
        .pipe(htmlreplace({
            css: 'styles/styles.css',
            js: 'js/combined.js'
        }))
        .pipe(gulp.dest(config.dest + ''));
});

gulp.task('updateImages', function () {
    'use strict';
    update('images');
});

gulp.task('updateScripts', function () {
    'use strict';
    update('scripts');
});

gulp.task('updateStyles', function () {
    'use strict';
    update('styles');
});

gulp.task('updateEmails', function () {
    'use strict';

    if (config.marketo.emails.update === true) {
        var updateEmailsCallback = function _updateEmailsCallback(token) {
            config.marketo.emails.files.forEach(function (element, index, array) {
                updateContent(token, 'email', element);
            });
        };

        getToken(updateEmailsCallback);
    }
});

gulp.task('updateLandingPages', function () {
    'use strict';

    if (config.marketo.landingPages.update === true) {
        var updateLandingPagesCallback = function _updateLandingPagesCallback(token) {
            config.marketo.landingPages.files.forEach(function (element, index, array) {
                updateContent(token, 'landingPageTemplate', element);
            });
        };

        getToken(updateLandingPagesCallback);
    }
});

// default gulp task
gulp.task('default', config.default);

// watch for file updates, run appropriate task
gulp.task('watch', function () {
    'use strict';

    livereload.listen();

    if (config.watch.babel) {
        gulp.watch(config.src + '/js/babel/**/*.js', ['babel']);
    }

    if (config.watch.html) {
        gulp.watch(config.src + '/**/*.html', ['html', 'updateLandingPages']);
    }

    if (config.watch.images) {
        gulp.watch(config.src + '/img/**/*.png', ['images', 'updateImages']);
    }

    if (config.watch.sass) {
        gulp.watch(config.src + '/styles/sass/**/*.scss', ['sass']);
    }

    if (config.watch.scripts) {
        gulp.watch(config.src + '/js/**/*.js', ['scripts', 'updateScripts']);
    }

    if (config.watch.styles) {
        gulp.watch(config.src + '/styles/css/**/*.css', ['styles', 'updateStyles']);
    }
});