/*jslint
browser, fudge
*/
/*global
console, document, window, jQuery, MktoForms2, $, main, alert, MutationObserver
*/
var main = (function() {
	'use strict';
	var _bootstrapify, _fixCheckboxes, _fixRadio, _initialize, _initializeForm, _mktoOnSuccess, _mktoOnValidate, _observe, _updateVisible;
	_bootstrapify = function bootstrapify() {
		// Remove mktoForm class to render Marketo's default styles useless.
		$('form').removeClass('mktoForm');
		// Remove superfluous DOM elements.
		$('.mktoOffset, .mktoGutter, .mktoClear').remove();
		// Remove any and all inline styles (TODO will have to be done onresize as well)
		$('form').removeAttr('style');
		$('form').find('*[style]').removeAttr('style');
		// Add Bootstrap form classes to form elements
		$('.mktoFormRow, .mktoButtonRow').addClass('form-group').addClass('row');
		// Marketo didn't specify correctly for .mktoButtonRow, so undo that here.
		$('.mktoButtonRow').css('display', 'block');
		$('input:not(:checkbox, :radio, :hidden), select, textarea').addClass('form-control');
		$('.mktoButton').addClass('btn');
		// $('.mktoFormCol, .mktoFieldWrap').addClass('col-sm-12');
		// Allow for multiple form columns within a row.
		$('.mktoFormRow').each(function(index, value) {
			var cols;
			cols = $(value).find('.mktoFormCol').length;
			if (cols > 1) {
				$(value).find('.mktoFormCol').removeClass('col-sm-12').addClass('multicol').addClass('col-md-' + (12 / cols));
				$(value).children('div.multicol:not(:first,:last)').css('padding', '0');
				$(value).children('div.multicol:last').css('padding-left', '0');
				$(value).children('div.multicol:first').css('padding-right', '0');
			}
		});
	};
	// TODO Move these functions into Bootstrapify? They are dependent on it.
/* _fixCheckboxes = function fixCheckboxes() {
        $('input:checkbox').each(function (index, value) {
            var cbParent,
                cbLabelContent,
                cbLabel;

            cbParent = $(value).parent().parent();
            cbLabelContent = cbParent.children('label').html();
            cbParent.children('label').remove();
            cbParent.children().children('label').html(cbLabelContent);
            cbLabel = $(value).next();
            $(value).detach();
            $(cbLabel).prepend(value);
            $(cbLabel).wrap('<div class="checkbox"></div>');
        });
    }; */
/* _fixRadio = function fixRadio() {
        $('input:radio').each(function (index, value) {
            var rbLabel;
            rbLabel = $(value).next();
            $(value).detach();
            $(rbLabel).prepend(value);
            $(rbLabel).wrap('<div class="radio"></div>');
        });
    }; */
	_initialize = function initialize() {
/* _observe('mktoForm_175', 'State', function () {
            alert('State added');
        }); */
	};
	_initializeForm = function initializeForm() {
		$('div.mktoForm').removeClass('mktoForm');
		// _bootstrapify();
		// _fixCheckboxes();
		// _fixRadio();
		$('form, div.mktoForm').removeClass('mktoForm');
		$('form').css('text-align', 'left');
		$('.mktoOffset, .mktoGutter, .mktoClear, .mktoAsterix').remove();
		$('form').removeAttr('style');
		$('form').find('*[style]').removeAttr('style');
		$('.mktoFormRow').addClass('form-group').addClass('row');
		$('form').addClass('form-horizontal');
		// $('form').find('.mktoFormCol').addClass('col-md-6 clearfix');
		$('.mktoFormRow').each(function(index, value) {
			var cols;
			cols = $(value).find('.mktoFormCol').length;
			if (cols > 1) {
				$(value).find('.mktoFormCol').addClass('multicol').addClass('clearfix col-md-' + (12 / cols));
				$(value).children('div.multicol:not(:first,:last)').css('padding', '0');
				$(value).children('div.multicol:last').css('padding', '0');
				$(value).children('div.multicol:first').css('padding', '0');
			} else {
				$(value).find('.mktoFormCol').addClass('clearfix');
			}
		});
		$('form').find('label').addClass('col-xs-4 control-label');
		$('input:not(:checkbox, :radio, :hidden), select, textarea').addClass('form-control');
		$('input.form-control').wrap('<div class="col-xs-8"></div>');
		$('select').prev('label').remove();
		$('select').parent().addClass('col-md-12');
		$('.mktoButton').addClass('btn btn-block');
		if ($('.form-group:last .mktoHtmlText').length > 0){
		$('.mktoHtmlText').parents('.mktoFormCol').addClass('col-md-6');
		var bR = $('.mktoButtonRow')
		bR.detach();
		$('.form-group:last .col-md-6').after(bR);
		$('.mktoButtonRow').addClass('col-md-6');
		$('.form-group:last .col-md-6').find('.mktoHtmlText').addClass('col-xs-12');
		}
		if ($(window).width() > 767) {
			var formHeight = $('.hero-form').outerHeight();
			$('.hero-txt').height(formHeight);
		}
		var resizeTimer;
		$(window).on('resize', function(e) {
			clearTimeout(resizeTimer);
			resizeTimer = setTimeout(function() {
				$('form').removeAttr('style');
				$('form').find('*[style]').removeAttr('style');
				if ($(window).width() > 767) {
					var formHeight = $('.hero-form').outerHeight();
					$('.hero-txt').height(formHeight);
				}
			}, 100);
		});
		// Utilize Chosen jQuery plugin on multi-select fields (https://github.com/harvesthq/chosen).
		// $('select[multiple=multiple]').chosen();
		_initialize();
		// TODO: How to prevent displaying the page until all JS is run.
		// document.getElementsByTagName("html")[0].style.visibility = "visible";
	};
	_mktoOnSuccess = function mktoOnSuccess() {};
	_mktoOnValidate = function mktoOnValidate(form) {
		// custom validation if necessary
		// return true or false
		return true;
	};
	_observe = function observe(container, id, callback) {
		var config, observer, target;
		if (window.hasOwnProperty('MutationObserver')) {
			target = document.getElementById(container);
			config = {
				attributes: false,
				childList: true,
				characterData: false,
				subtree: true
			};
			observer = new MutationObserver(function(mutations) {
				var newNode = document.getElementById(id);
				if (typeof newNode === 'object') {
					callback();
				}
			});
			observer.observe(target, config);
		} else {
			jQuery(document).on('DOMNodeInserted', function(elem) {
				if (elem.target.id === id) {
					callback();
				}
			});
		}
	};
	_updateVisible = function updateVisible(hideable) {
		$.each(hideable, function(index, value) {
			if (value.show === 'false') {
				$(value.id).remove();
				if (value.updateId !== undefined && value.updateId.length > 0) {
					$(value.updateId).removeClass(value.removeClass).addClass(value.addClass);
				}
			}
		});
	};
	return Object.freeze({
		initialize: _initialize,
		initializeForm: _initializeForm,
		mktoOnSuccess: _mktoOnSuccess,
		mktoOnValidate: _mktoOnValidate
	});
}());